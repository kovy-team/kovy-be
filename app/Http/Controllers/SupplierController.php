<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = DB::table('suppliers')
            ->where('del_flg', 0)
            ->get();

        return response()->json($suppliers, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $supplier = new Supplier();

        $supplier->name = $req['name'];
        $supplier->address = $req['address'];
        $supplier->phone = $req['phone'];
        $supplier->email = $req['email'];
        $supplier->fax = $req['fax'];
        $supplier->del_flg = 0;


        $supplier->save();

        return response()->json($supplier, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $supplier = Supplier::where('id', $req['id'])->first();

        $supplier->name = $req['name'];
        $supplier->address = $req['address'];
        $supplier->phone = $req['phone'];
        $supplier->email = $req['email'];
        $supplier->fax = $req['fax'];
        $supplier->del_flg = 0;

        $supplier->save();

        return response()->json($supplier, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        $supplier->del_flg = 1;
        $supplier->save();

        return response()->json($supplier, 200);
    }
}
