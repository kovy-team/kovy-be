<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function productWomen($category)
    {
        // DB::enableQueryLog();
        $products = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('categories.id', $category)
            ->where('products.del_flg', 0)
            ->select(
                'products.id',
                'products.name as product',
                'categories.name as categoryName',
                'products.price',
                'products.image',
            )
            ->limit(4)
            ->get();

        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }
        // return DB::getQueryLog();
        return response()->json($products, 200);
    }

    public function productMen($category)
    {
        $products = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('categories.id', $category)
            ->where('products.del_flg', 0)
            ->select(
                'products.id',
                'products.name as product',
                'categories.name as categoryName',
                'products.price',
                'products.image',
            )
            ->limit(4)
            ->get();

        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }
        return response()->json($products, 200);
    }

    public function productKid($category)
    {
        $products = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('categories.id', $category)
            ->where('products.del_flg', 0)
            ->select(
                'products.id',
                'products.name as product',
                'categories.name as categoryName',
                'products.price',
                'products.image',
            )
            ->limit(4)
            ->get();

        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }
        return response()->json($products, 200);
    }

    public function products($category)
    {
        if ($category === 'product') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                    'products.content',
                    'products.supplier_id',
                    'products.category_id',
                    'products.total'
                )
                ->limit(20)
                ->get();
        } else if ($category === 'productMen') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [5, 6, 7, 8])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else if ($category === 'productWomen') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [1, 2, 3, 4])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else if ($category === 'productKid') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [9, 10, 11, 12])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else if ($category === 'shirt') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [1, 3, 9])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else if ($category === 'shawl') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [2, 6, 10])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else if ($category === 'handBag') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [3, 7, 11])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else if ($category === 'jewelry') {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->whereIn('categories.id', [4, 8, 12])
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        }


        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }
        return response()->json($products, 200);
    }

    public function search(Request $req)
    {
        if ($req['search'] === null) {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        } else {
            $products = DB::table('products')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('products.del_flg', 0)
                ->where('products.name', 'like', "%{$req['search']}%")
                ->select(
                    'products.id',
                    'products.name as product',
                    'categories.name as categoryName',
                    'products.price',
                    'products.image',
                )
                ->limit(20)
                ->get();
        }

        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }
        return response()->json($products, 200);
    }

    public function getCart($user_id)
    {
        $number = DB::table('users')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->join('products', 'orders.product_id', '=', 'products.id')
            ->where('users.id', $user_id)
            ->select('products.id', 'products.name', 'products.price', 'products.image', 'orders.quantity')
            ->get();

        foreach ($number as $num) {
            if ($num->image) {
                $image = 'storage/products/' . $num->image;
                $num->image = asset($image);
            }
        }

        return response()->json($number, 200);
    }

    public function updateCart(Request $req)
    {

        $orders = DB::table('orders')
            ->where('user_id', $req['id_user'])
            ->where('product_id', $req['product']['id'])
            ->get();

        if ($orders->count() > 0) {
            $order = DB::table('orders')
                ->where('user_id', $req['id_user'])
                ->where('product_id', $req['product']['id'])
                ->update(['quantity' =>   (int)$orders[0]->quantity + 1]);

            return response()->json($order, 200);
        } else {
            $order = new Order();
            $order->product_id = $req['product']['id'];
            $order->user_id = $req['id_user'];
            $order->transaction_id = 0;
            $order->amount = 1;
            $order->quantity = 1;

            $order->save();
            return response()->json($order, 200);
        }
    }

    public function downCart(Request $req)
    {
        $order = DB::table('orders')
            ->where('user_id', $req['id_user'])
            ->where('product_id', $req['product']['id'])
            ->update(['quantity' => $req['product']['quantity']]);

        return response()->json($order, 200);
    }

    public function deleteCart(Request $req)
    {
        $order = DB::table('orders')
            ->where('user_id', $req['id_user'])
            ->where('product_id', $req['id_product'])
            ->delete();

        return response()->json($order, 200);
    }

    public function getProduct($id)
    {
        $products = DB::table('products')
            ->where('products.id', $id)
            ->where('del_flg', 0)
            ->select(
                'products.*'
            )
            ->get();

        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }

        return response()->json($products, 200);
    }

    public function numberCart($id)
    {
        $num = Order::where('user_id', $id)->sum('quantity');

        return response()->json((int)$num, 200);
    }

    public function category()
    {
        $category = DB::table('categories')
            ->select(
                'id',
                'name'
            )
            ->get();

        return response()->json($category, 200);
    }

    public function saveImgBase64($param, $folder)
    {
        list($extension, $content) = explode(';', $param);
        $tmpExtension = explode('/', $extension);
        preg_match('/.([0-9]+) /', microtime(), $m);
        $fileName = sprintf('img%s%s.%s', date('YmdHis'), $m[1], $tmpExtension[1]);
        $content = explode(',', $content)[1];
        $storage = Storage::disk('public');

        $checkDirectory = $storage->exists($folder);

        if (!$checkDirectory) {
            $storage->makeDirectory($folder);
        }

        $storage->put($folder . '/' . $fileName, base64_decode($content), 'public');

        return $fileName;
    }

    public function create(Request $req)
    {
        $product = new Product();
        $product->supplier_id = $req['supplier_id'];
        $product->category_id = $req['category_id'];
        $product->name = $req['name'];
        $product->price = $req['price'];
        $product->content = $req['content'];
        $product->total = $req['total'];
        $image = self::saveImgBase64($req->get('image'), "products");
        $product->image = $image;

        $product->save();

        return response()->json($product, 200);
    }

    public function update(Request $req)
    {
        $product = Product::where('id', $req['id'])->first();

        $product->supplier_id = $req['supplier_id'];
        $product->category_id = $req['category_id'];
        $product->name = $req['name'];
        $product->price = $req['price'];
        $product->content = $req['content'];
        $product->total = $req['total'];
        $image = self::saveImgBase64($req->get('image'), "products");
        $product->image = $image;

        $product->save();

        return response()->json($product, 200);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->del_flg = 1;
        $product->save();

        return response()->json($product, 200);
    }

    public function getProductAll(Request $req)
    {
        $productCount = DB::table('products')
            ->where('del_flg', 0)
            ->get()
            ->count();

        $totalPage = (int)((($productCount - 1) / 10) + 1);


        $products = DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('products.del_flg', 0)
            ->select(
                'products.id',
                'products.name as product',
                'products.price',
                'products.image',
                'products.content',
                'products.supplier_id',
                'products.category_id',
                'products.total',
                'categories.name as categoryName'
            )
            ->skip(10 * (int)(($req['page']) - 1))
            ->limit(10)
            ->get();

        foreach ($products as $product) {
            if ($product->image) {
                $image = 'storage/products/' . $product->image;
                $product->image = asset($image);
            }
        }
        return response()->json(['data' => $products, 'totalPage' => $totalPage], 200);
    }

    public function topProduct()
    {
        $top =  DB::table('products')
            ->join('categories', 'products.category_id', '=', 'categories.id')
            ->where('products.del_flg', 0)
            ->select(
                'products.id',
                // 'products.image',
                'products.name',
                'products.price',
                'products.total',
                'categories.name as category_name'
            )
            ->limit(20)
            ->get();


        return response()->json($top, 200);
    }

    public function proceedPayment(Request $req)
    {
        $proceed = Order::where('user_id', $req['id_user'])->delete();

        return response()->json($proceed, 200);
    }
}
