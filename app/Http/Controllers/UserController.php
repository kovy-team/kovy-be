<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')
            ->get();

        foreach ($users as $user) {
            if ((int)$user->sex === 1) {
                $user->sex = 'Nam';
            } else if ((int)$user->sex === 2) {
                $user->sex = 'Nữ';
            } else {
                $user->sex = 'Không xác định';
            }
        }

        return response()->json($users, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = \Validator::make(
            $request->all(),
            [
                'email' => 'required|unique:users,email'
            ],
            [
                'unique' => 'Email already exist',
                'requred' => 'sasaxa'
            ]
        )->validate();

        $user = new User();
        $user->email = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->token = Str::random(80);
        $user->save();

        return $user;
    }

    public function login(Request $req)
    {

        if ($req['role'] === 'admin') {
            $admin = Admin::where('email', $req['email'])->first();

            if ($admin && Hash::check($req['password'], $admin->password)) {
                return response()->json(['Login successfully.', 'user' => $admin], 200);
            } else {
                return response()->json(['no_registration_confirmation'], 404);
            }
        } else {
            $user = User::where('email', $req['email'])->first();

            if ($user && Hash::check($req['password'], $user->password)) {
                return response()->json(['Login successfully.', 'user' => $user], 200);
            }
        }
        return response()->json('Loggin error', 404);
    }

    public function auth(Request $req)
    {
        if ($req['role'] === 'admin') {
            $auth = Admin::where('email', $req['email'])->first();

            if ($req['token'] === $auth->token) {
                return response()->json(['success'], 200);
            }
            return response()->json(['error'], 404);
        } else {
            $auth = User::where('email', $req['email'])->first();

            if ($req['token'] === $auth->token) {
                return response()->json(['success'], 200);
            }
            return response()->json(['error'], 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $rq)
    {
        return response()->json(['Login successfully.', 'user' =>User::where('email', $rq["email"])->first()], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user =  User::where('email', $request['email'])->first();;
        $user->phone = $request['phone'];
        $user->address = $request['address'];
        $user->name = $request['name'];
        $user->sex = $request['sex'];
        
        $user->save();
        return response()->json(['success'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
