<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\PublisherController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UserController;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', function () {
    return ['message' => 'hello'];
});

//register
Route::post('/register', [UserController::class, 'create']);
//login
Route::post('/login', [UserController::class, 'login']);
//userinfo
Route::post('/userinfo', [UserController::class, 'show']);
//update userinfo
Route::post('/userinfo/update', [UserController::class, 'update']);
//product Women
Route::get('/productWomen/{category}', [ProductController::class, 'productWomen']);
//product Men
Route::get('/productMen/{category}', [ProductController::class, 'productMen']);
//product Kid
Route::get('/productKid/{category}', [ProductController::class, 'productKid']);
//product by category
Route::get('/products/{category}', [ProductController::class, 'products']);
//search
Route::post('/search', [ProductController::class, 'search']);
//get cart
Route::get('/cart/{user_id}', [ProductController::class, 'getCart']);
//update cart
Route::post('/cart/update', [ProductController::class, 'updateCart']);
//update cart
Route::post('/cart/down', [ProductController::class, 'downCart']);
//update cart
Route::post('/deleteCart', [ProductController::class, 'deleteCart']);
//get product by id
Route::get('/getProduct/{id}', [ProductController::class, 'getProduct']);
//authenticate
Route::post('/auth', [UserController::class, 'auth']);
//number cart
Route::get('/numberCart/{id}', [ProductController::class, 'numberCart']);

//ADMIN
//get category
Route::get('/category', [ProductController::class, 'category']);
//get publisher
Route::get('/supplier', [SupplierController::class, 'index']);
//add supplier
Route::post('/addSupplier', [SupplierController::class, 'create']);
//update supplier
Route::put('/updateSupplier', [SupplierController::class, 'edit']);
// Delete Supplier
Route::delete('deleteSupplier/{id}', [SupplierController::class, 'destroy']);
// get all user
Route::get('getAllUser', [UserController::class, 'index']);
//create product
Route::post('addProduct', [ProductController::class, 'create']);
//create product
Route::put('updateProduct', [ProductController::class, 'update']);
//create product
Route::delete('deleteProduct/{id}', [ProductController::class, 'destroy']);

//get Product All
Route::post('getProductAll', [ProductController::class, 'getProductAll']);
//get top product
Route::get('topProduct', [ProductController::class, 'topProduct']);

Route::post('proceedPayment', [ProductController::class, 'proceedPayment']);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
